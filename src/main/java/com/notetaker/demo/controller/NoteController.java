package com.notetaker.demo.controller;

import com.notetaker.demo.entity.Note;
import com.notetaker.demo.exceptions.ResourceNotFoundException;
import com.notetaker.demo.repository.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class NoteController {

    @Autowired
    NoteRepository noteRepository;

    // Get all notes
    @GetMapping("/notes")
    public List<Note> getNotes() {
        return noteRepository.findAll();
    }

    // Create a new Note
    @PostMapping("/notes")
    public Note createNote(@Valid @RequestBody Note note) {
        return noteRepository.save(note);
    }


    // Get single note
    @GetMapping("/notes/{id}")
    public Note getNoteById(@PathVariable(value = "id") Long id) {
        return noteRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Note", "id", id));
    }

    // Update single note
    @PutMapping("/notes/{id}")
    public Note updateNote(@PathVariable(value = "id") Long id,
                           @Valid @RequestBody Note note) {
        Note updatedNote = noteRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Note", "id", id));

        updatedNote.setTitle(note.getTitle());
        updatedNote.setContent(note.getContent());
        updatedNote.setImage(note.getImage());

        return noteRepository.save(updatedNote);
    }

    // Delete note
    @DeleteMapping("/notes/{id}")
    public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long id) {

        Note note = noteRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Note", "id", id));

        noteRepository.delete(note);

        return ResponseEntity.ok().build();
    }
}
